package animales;
/**
 * Clase que representa un animal por su nombre, numero de patas y ojos.
 * @author Miquel
 */
public class Animal 
{
  // Visibilidad de los atributos.
  public String nombre;
  protected int patas;
  private int ojos;
  
  // Sobrecarga de constructores.
  public Animal(String nombre, int patas, int ojos) {
    this.nombre = nombre;
    this.patas = patas;
    this.ojos = ojos;
  }
  
  public Animal(int patas, int ojos) {
    nombre = "animal";
    this.patas = patas;
    this.ojos = ojos;
  }
  
  // Metodos que definen el comportamiento.
  public void setPatas(int patas) {
    this.patas = patas;
  }
  
  public int getPatas() {
    return patas;
  }
  
  public void setOjos(int ojos) {
    this.ojos = ojos;
  }
  
  public int getOjos() {
    return ojos;
  }
  
  public String habla() { 
     return "Soy un " + nombre + " " + " de " + patas + " patas y " + ojos + " ojos"; 
  } 
}

