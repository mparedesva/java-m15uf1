package animales;

import progoo.*;
import java.util.Arrays;

public class ExempleHerenciaAnimals {
    
    public static void main(String[] args) {
        
        String salutacio = "Prova POO Animals.";
        System.out.println(salutacio);
        
        Animal animal = new Animal("Perro",4,2);
        Animal gato = new Gato("Gato",4,2);
        Animal loro = new Loro("Loro",0,2);

        System.out.println(salutacio);
        Animal[] animales = {animal,gato,loro};
        for (Animal animal1 : animales) {
            System.out.println(animal1.habla());
        }
        
        
    }
}
