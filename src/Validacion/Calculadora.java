package Validacion;

import animales.*;

/**
 * Clase que representa una calculadora que opera amb 2 numeros reals.
 * @author Miquel
 */
public class Calculadora
{

  public Calculadora() {
      
  }
  
  public double suma(double num1, double num2) {
     return num1+num2;
  }
  
  public double resta(double num1, double num2) {
      return num1-num2;
  }
  
  public double mult(double num1, double num2) {
      return num1*num2;
  }
  
  public double div(double num1, double num2) {
      if(num2>0)
        return num1/num2;
      else
        return 0;
  }
}

