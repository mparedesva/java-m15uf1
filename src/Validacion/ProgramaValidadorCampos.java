package Validacion;

import animales.*;

/**
 * 23. Crea una classe Validador que servirà per verificar camps de formularis;
 * i una classe Main que provi tots els mètodes. Aquests seran: 
 * -validaCampNoBuit(String camp) 
 * -validaTamanyMaximCamp(String camp, int tamanyMaxCamp) 
 * -validaCampEmail(String camp) 
 * - validaIntervalNumero(int intMinim, int intMaxim)
 */
public class ProgramaValidadorCampos {

    static final String SI = "Sí";
    static final String NO = "No";
    static final String CAMP_BUIT = "El camp està buit?";
    static final String CAMP_INTERNVAL = "El camp sobrepassa l'interval indicat?";
    static final String CAMP_EMAIL = "El camp és un email vàlid?";
    
    // Converteix un boolea en una resposta de Si o No que mostra 
    // per pantalla.
    private static void imprimeixResposta(boolean respostaAfirmativa) {
        if(respostaAfirmativa)
            System.out.println(SI);
        else
            System.out.println(NO);
    }
    
    public static void main(String[] args) {

        ValidadorCampos validador = new ValidadorCampos();
        System.out.println(CAMP_BUIT);
        imprimeixResposta(validador.validaCampNoBuit("Bones")); 
        System.out.println(CAMP_BUIT);
        imprimeixResposta(validador.validaCampNoBuit("")); 
        
        System.out.println(CAMP_INTERNVAL);
        imprimeixResposta(validador.validaTamanyMaximCamp("Nombre",20));
        System.out.println(CAMP_INTERNVAL);
        imprimeixResposta(validador.validaTamanyMaximCamp("456789",5));
        
        System.out.println(CAMP_EMAIL);
        imprimeixResposta(validador.validaCampEmail("Nombre"));
        System.out.println(CAMP_EMAIL);
        imprimeixResposta(validador.validaCampEmail("miquel.angel.amoros@gmail.com"));
   }
  
}
