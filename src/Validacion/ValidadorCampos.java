package Validacion;

/** 
23. 
* Crea una classe Validador que servirà per verificar camps de formularis; 
* i una classe Main que provi tots els mètodes. Aquests seran:
    - validaCampNoBuit(String camp)
    - validaTamanyMaximCamp(String camp, int tamanyMaxCamp)
    - validaCampEmail(String camp)
    - validaIntervalNumero(int intMinim, int intMaxim)
*/
public class ValidadorCampos
{

  public ValidadorCampos() {
      
  }
  
  public boolean validaCampNoBuit(String camp) {
     return camp !=null && camp.isEmpty(); // Otras opciones, camp.equals("");
  }
  
  public boolean validaTamanyMaximCamp(String camp, int tamanyMaxCamp) {
     return camp.length()>tamanyMaxCamp;
  }
    
  public boolean validaCampEmail(String email) {
     // https://www.tutorialspoint.com/validate-email-address-in-java 
     String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
     return email.matches(regex);
  }
  
  public boolean validaIntervalNumero(int camp, int numMin, int numMax) {
     return true;
  }
  
}

