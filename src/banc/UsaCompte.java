package banc;

import Validacion.*;
import animales.*;
import progoo.*;
import java.util.Arrays;

/**
 * Crea una classe Compte i una classe que utilitzi l’objecte Compte.
Les seves propietats: 
	String NomTitular 	
	double diners
Els seus mètodes:
	CrearCompte()
	CrearCompte(diners)
	CanviarNomTitular(nomTitular)
	FicarDiners(diners)
	TreureDiners(diners)
	compteOrigen.TraspasDiners(compteDesti)
 * @author mique
 */
public class UsaCompte {
    
    public static void main(String[] args) {
        
        String salutacio = "Prova POO Comptes.";
        System.out.println(salutacio);
        
        Compte compteAnonim = new Compte(1450.0);
        Compte compteDiari = new Compte("Miquel",5450.0);
        System.out.println(compteDiari.dadesCompte());
        compteDiari.TreureDiners(1000);
        System.out.println(compteDiari.dadesCompte());
        compteDiari.FicarDiners(5000);
        
        System.out.println(compteAnonim.dadesCompte());
        // Traspas compte 
        System.out.println(compteDiari.dadesCompte());
        System.out.println("Traspas dinero ");
        compteDiari.traspasDiners(compteAnonim,450.0);
        System.out.println(compteDiari.dadesCompte());
        System.out.println(compteAnonim.dadesCompte());
        
    }
}
