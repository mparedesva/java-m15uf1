package banc;

/**
 * Crea una classe Compte i una classe que utilitzi l’objecte Compte. Les seves
 * propietats: String NomTitular double diners Els seus mètodes: CrearCompte()
 * CrearCompte(diners) CanviarNomTitular(nomTitular) FicarDiners(diners)
 * TreureDiners(diners) compteOrigen.TraspasDiners(compteDesti)
 *
 * @author tarda
 */
public class Compte {

    private String nomTitular;
    private double diners;

    public Compte() {

    }

    public Compte(double diners) {
        this.nomTitular = "Anonim";
        this.diners = diners;
    }

    public Compte(String nomTitular, double diners) {
        this.nomTitular = nomTitular;
        this.diners = diners;
    }

    public void FicarDiners(double diners) {
        this.diners += diners;
    }

    public void TreureDiners(double diners) {
        if(pucTreureDiners(diners))
            this.diners -= diners;
        else
            System.out.println("No puedes sacar más dinero del que tienes.");
    }

    public void CanviarNomTitular(String nomTitular) {
        this.nomTitular = nomTitular;
    }

    public double ConsultaSaldo() {
        return this.diners;
    }

    public String dadesCompte() {
        return "Dades Compte{" + "nomTitular=" + nomTitular + ", diners=" + diners + '}';
    }

    public void traspasDiners(Compte compteDesti, double dinersATreure) {
        
// if(pucTreureDiners(this.diners)) {
        if(pucTreureDiners(dinersATreure)) {
            this.diners -= dinersATreure;
            compteDesti.diners += dinersATreure;
        } else {
            System.out.println("No puedes sacar más dinero del que tienes.");
        }
    }
    
    public boolean pucTreureDiners(double dinersATreure) {
        return this.diners > dinersATreure;
    }

}
