package ValFechas;



import java.util.regex.Pattern;

/**
 * This class verify a date and calculate the next date
 * @author Pepe
 * @version 2.0 2017-10-11
 */
class validateDate {
    
    private int dia=0;
    private int mes=0;
    private int any=0;

    public validateDate() {
        
    }
    public int getDia(){
        return this.dia;
    }
    
    public int getMes(){
        return this.mes;
    }

    public int getAny(){
        return this.any;
    }

    /**
     * 
     * @param data is a string with a day
     * @return flag say if a day is valid
     */
    public boolean validarFecha(String data){
        
        boolean flag=false;
        //valida formato        
        if(Pattern.matches("\\d{1,2}/\\d{1,2}/\\d{1,4}", data)){
            //divide la fecha en dia, mes,año
            String[] ff = data.split("/");
            //valida año -> solo años entre 1900 y 2100
            this.any = Integer.parseInt(ff[2]);
            if(this.any>=1900 && this.any<=2100){
                //valida mes -> 1 a 12
                this.mes = Integer.parseInt(ff[1]);
                if(this.mes>=1 && this.mes<=12){
                    //valida dia entre 1 y la cantidad de dias que tenga ese mes 
                    this.dia = Integer.parseInt(ff[0]);
                    if(this.dia>=1 && this.dia<=cantidad_dias(this.mes)){
                        flag=true;
                    }
                }
            }
        }
        return flag;
    }

 /**
  * 
  * @return result is the next day
  */
    public String nextDay(){
        String result="";
        if(dia < cantidad_dias(mes)){
            dia++;
        }else{
            if(mes < 12){
                mes++;
                dia=1;
            }else{//si es diciembre, se pasa al año siguiente
                any++;
                mes=1;
                dia=1;
            }
        }
        
        if(dia <10) result="0"+dia;
        else result= ""+dia;
        if(mes<10) result += "/0"+mes;
        else result += "/"+mes;
        
        result = result + "/" + any;
        return result;
    }

    /**
     * 
     * @param m is the month
     * @return d is the total days of the month
     */
    private int cantidad_dias(int m){
        int d =0;
        switch(m) {
            case 1: 
            case 3: 
            case 5: 
            case 7:
            case 8:    
            case 10: 
            case 12: 
              d=31;
              break;
        case 2:
              d=28;
              break;
        case 4: 
        case 6: 
        case 9: 
        case 11:
              d=30; 
              break;
        }
        return d;
    }

}
    
    
    

