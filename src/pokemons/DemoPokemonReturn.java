package pokemons;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DemoPokemonReturn {

    private static int pokJugador;

    public static List<Pokemon> crearPokemons() {
        List<Pokemon> pokemons = new ArrayList<>();
        pokemons.add(new Pokemon(1,"Bulbasaur",TipoPokemon.PLANTA,5,60,80,60));
        pokemons.add(new Pokemon(2,"Picachu",TipoPokemon.ELECTRICO,5,70,50,90));
        pokemons.add(new Pokemon(3,"Charmader",TipoPokemon.FUEGO,5,90,60,80));
        pokemons.add(new Pokemon(4,"Squirtle",TipoPokemon.AGUA,5,80,90,50));
        return pokemons;
    }
    
    private static boolean combatePokemons(Pokemon jugador, Pokemon maquina) {
        // TODO Controlar velocidad.
        // if(jugador.getVelocidad()>maquina.getVelocidad()) {
        
        double[][] pokemonBattleChart = {
            // 0.ELECTRICO,1.FUEGO,2.AGUA,3.PLANTA
            {0.5,1.0,2.0,0.5},
            {1.0,0.5,0.5,2.0},
            {0.5,2.0,1.0,0.5},
            {1.0,0.5,2.0,0.5}
        };
        
        int posJug = jugador.getTipo().pokOrdinal;
        int posMaq = maquina.getTipo().pokOrdinal;
        System.out.println(maquina.getTipo());
        double multiplicador = pokemonBattleChart[posJug][posMaq];
        Double nuevoAtaque = jugador.getAtaque() * multiplicador;
        jugador.setAtaque( nuevoAtaque.intValue() );
        System.out.println(jugador.getNombre() + " de tipo " + jugador.getTipo() + " ataca a ");
        System.out.println(maquina.getNombre() + " de tipo " + maquina.getTipo());
        if(multiplicador==2.0) {
            System.out.println("Ha sido super efectivo");
            return true;
        }
        if(multiplicador==0.5) {
            System.out.println("No ha sido muy efectivo"); 
            return false;
        }
        System.out.println(jugador.getAtaque()+">"+maquina.getAtaque());
        return jugador.getAtaque()>maquina.getAtaque();
    }
    
    private static void menuJuegoPokemons(List<Pokemon> pokemons) {
        System.out.println("Elige tu pokemon.");
        for (Pokemon pokemon : pokemons) {
            System.out.println(pokemon.getId() + " - " + pokemon.getNombre());
        }
        System.out.println(pokemons.size()+1 + " - Salir");
    }
    
    private static String[] mensajes = {   
            "Elige tu pokemon.",
            "Tu pokemon ha ganado.", 
            "Tu pokemon ha perdido."
   };
    
    
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        List<Pokemon> pokemons = crearPokemons();
        // new ArrayList<>();
        // List pokemons
        menuJuegoPokemons(pokemons);
        pokJugador = lector.nextInt();
        boolean ganador = true;
        while(pokJugador != pokemons.size()+1) {
            int pokMaquina = (int) (Math.random() * 3) + 1;

            ganador = combatePokemons(pokemons.get(pokJugador-1),pokemons.get(pokMaquina));
            
            System.out.println(ganador?mensajes[1]:mensajes[2]);
            
            menuJuegoPokemons(pokemons);
            pokJugador = lector.nextInt();
        }
    }
    
}

