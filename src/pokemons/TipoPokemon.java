package pokemons;

public enum TipoPokemon {
    ELECTRICO(0),FUEGO(1),AGUA(2),PLANTA(3);
    int pokOrdinal = 0;

    TipoPokemon(int ord) {
        this.pokOrdinal = ord;
    }
}
